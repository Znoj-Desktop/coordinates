#include <stdio.h>
#include <stdlib.h>

// ofset HAS TO BE BIGGER than number of lines in input file due to function removeIdFromMatrix and getBiggestArea!!!!!!
const int COUNTED_COORDINATES_OFFSET = 10;
const int INITIAL_ARRAY_SIZE = 100;

bool readInputFile(FILE *inputFile, const char* inputFileName, int &arraySize, int **coordinatesArray, int &sizeCA, int &maxX, int &maxY) {
	if (fopen_s(&inputFile, inputFileName, "r") != 0) {
		return false;
	}

	int xCoordinate, yCoordinate;
	while (fscanf_s(inputFile, "%d, %d\n", &xCoordinate, &yCoordinate) != EOF) {
		coordinatesArray[sizeCA] = (int *) malloc(2 * sizeof(int));
		coordinatesArray[sizeCA][0] = xCoordinate;
		coordinatesArray[sizeCA][1] = yCoordinate;
		sizeCA++;
		// reallocate if needed
		if (sizeCA >= arraySize) {
			coordinatesArray = (int**) realloc(coordinatesArray, arraySize *sizeof(int));
			arraySize *= 2;
		}
		// detect max
		if (xCoordinate > maxX) {
			maxX = xCoordinate;
		}
		if (yCoordinate > maxY) {
			maxY = yCoordinate;
		}
	}
	return true;
}

void printOutArray(int **array, int size, int dimension) {
	for (int x = 0; x < size; x++) {
		if (array[x][0] == 0) {
			printf("  .");
		}
		else {
			printf("%3d", array[x][0]);
		}

		for (int y = 1; y < dimension; y++) {
			if (array[x][y] == 0) {
				printf("  .");
			}
			else {
				printf("%3d", array[x][y]);
			}
		}

		printf("\n");

	}
}

void createMatrix(int **matrix, int **coordinatesArray, int sizeCA, int maxX, int maxY) {
	// initializeMatrix
	for (int x = 0; x < maxX + 1; x++) {
		for (int y = 0; y < maxY + 1; y++) {
			matrix[x][y] = 0;
		}
	}

	//put coordinates into matrix
	for (int i = 0; i < sizeCA; i++) {
		matrix[coordinatesArray[i][0]][coordinatesArray[i][1]] = i + 1;
	}
}

int distanceBetweenCoordinates(int x1, int y1, int x2, int y2) {
	int x = x1 - x2;
	if (x < 0) {
		x *= -1;
	}

	int y = y1 - y2;
	if (y < 0) {
		y *= -1;
	}

	return x + y;
}

void processMatrix(int **matrix, int** coordinatesArray, int index2ca, int maxX, int maxY) {
	int shortestDistance;
	int shortestDistanceId = 0;
	int currentDistance;
	for (int x = 0; x < maxX + 1; x++) {
		for (int y = 0; y < maxY + 1; y++) {
			if (matrix[x][y] == 0) {
				shortestDistance = maxX + maxY;
				for (int i = 0; i < index2ca; i++) {
					currentDistance = distanceBetweenCoordinates(x, y, coordinatesArray[i][0], coordinatesArray[i][1]);
					if (currentDistance == shortestDistance) {
						shortestDistanceId = 0;
					}
					if (currentDistance < shortestDistance) {
						shortestDistanceId = i + 1 + COUNTED_COORDINATES_OFFSET;
						shortestDistance = currentDistance;
					}
				}
				matrix[x][y] = shortestDistanceId;
			}
		}
	}
}

// id will be removed from coordinatesArray as well for better performance when counting areas
int countAndRemoveIdFromMatrix(int **matrix, int **coordinatesArray, int sizeCA, int maxX, int maxY, int id) {
	int id2;

	if (id > COUNTED_COORDINATES_OFFSET) {
		id2 = id - COUNTED_COORDINATES_OFFSET;
		// remove id from coordinatesArray for better performance when counting areas
		coordinatesArray[id2-1][0] = -1;
		coordinatesArray[id2-1][1] = -1; // not important - checking just first, but removing both
	}
	else {
		id2 = id + COUNTED_COORDINATES_OFFSET;
		// remove id from coordinatesArray for better performance when counting areas
		coordinatesArray[id-1][0] = -1;
		coordinatesArray[id-1][1] = -1; // not important - checking just first, but removing both
	}

	int count = 0;
	for (int x = 0; x < maxX + 1; x++) {
		for (int y = 0; y < maxY + 1; y++) {
			if (matrix[x][y] == id || matrix[x][y] == id2) {
				count++;
				matrix[x][y] = 0;
			}
		}
	}
	return count;
}

void removeInfiniteAreas(int **matrix, int **coordinatesArray, int sizeCA, int maxX, int maxY) {
	for (int x = 0; x < maxX + 1; x++) {
		if (matrix[x][0] != 0) {
			countAndRemoveIdFromMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY, matrix[x][0]);
		}
	}
	for (int x = 0; x < maxX + 1; x++) {
		if (matrix[x][maxY] != 0) {
			countAndRemoveIdFromMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY, matrix[x][maxY]);
		}
	}
	for (int y = 1; y < maxY; y++) {
		if (matrix[0][y] != 0) {
			countAndRemoveIdFromMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY, matrix[0][y]);
		}
	}
	for (int y = 1; y < maxY; y++) {
		if (matrix[maxX][y] != 0) {
			countAndRemoveIdFromMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY, matrix[maxX][y]);
		}
	}
}

int getBiggestArea(int **matrix, int **coordinatesArray, int sizeCA, int maxX, int maxY, int &biggestAreaId) {
	int biggestArea = 0;
	int count = 0;
	for (int x = 1; x < maxX; x++) {
		for (int y = 1; y < maxY; y++) {
			int id = matrix[x][y];
			if (matrix[x][y] > COUNTED_COORDINATES_OFFSET) {
				id = matrix[x][y] - COUNTED_COORDINATES_OFFSET;
			}

			if (matrix[x][y] != 0 && coordinatesArray[id-1][0] != -1) {
				count = countAndRemoveIdFromMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY, matrix[x][y]);
				if (count > biggestArea) {
					biggestArea = count;
					biggestAreaId = id;
				}
			}
		}
	}
	return biggestArea;
}

void freeMemory(int** coordinatesArray, int index2ca) {
	for (int i = 0; i < index2ca; i++) {
		free(coordinatesArray[i]);
	}
	free(coordinatesArray);
}

int main() {
	// init
	printf("initialization...\n");
	FILE *inputFile = NULL;
	// could be argument
	const char * inputFileName = "test.txt";
	//const char * inputFileName = "coordinates_input.txt";

	// could be get from inputFile for better RAM usage, but worst performance due to multiple read file
	int arraySize = INITIAL_ARRAY_SIZE;
	int **coordinatesArray = (int **) malloc(arraySize * sizeof(int));
	int sizeCA = 0;

	int maxX = 0;
	int maxY = 0;
	printf("initialization done.\n\n");

	//input
	printf("reading file \'%s\'... \n", inputFileName);
	if (!readInputFile(inputFile, inputFileName, arraySize, coordinatesArray, sizeCA, maxX, maxY)) {
		fprintf(stderr, "Can not read input file \'%s\'.\n", inputFileName);
		system("pause");
		return 1;
	}
	printf("reading input done.\n\n");
	
	//print input array
	printf("input data: \n");
	printOutArray(coordinatesArray, sizeCA, 2);
	printf("\n");

	// creating matrix
	printf("creating matrix... \n");
	int **matrix = (int **)malloc((maxX+1) * sizeof(int));
	for (int i = 0; i < (maxX+1); i++) {
		matrix[i] = (int *)malloc((maxY+1) * sizeof(int));
	}
	createMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY);
	printf("creating matrix done.\n\n");

	//print matrix with coordinates in it
	printf("initialized matrix: \n");
	printOutArray(matrix, maxX+1, maxY+1);
	printf("\n");

	printf("processing matrix... \n");
	processMatrix(matrix, coordinatesArray, sizeCA, maxX, maxY);
	printf("processing matrix done.\n\n");

	//print matrix with coordinates in it
	printf("processed matrix: \n");
	printOutArray(matrix, maxX + 1, maxY + 1);
	printf("\n");

	printf("remove infinite areas from matrix... \n");
	removeInfiniteAreas(matrix, coordinatesArray, sizeCA, maxX, maxY);
	printf("remove infinite areas from matrix done.\n\n");

	//print matrix without infinite areas
	printf("matrix without infinite areas: \n");
	printOutArray(matrix, maxX + 1, maxY + 1);
	printf("\n");

	printf("count biggest area from matrix... \n");
	int biggestAreaId = -1;
	int result = getBiggestArea(matrix, coordinatesArray, sizeCA, maxX, maxY, biggestAreaId);
	printf("count biggest area from matrix done.\n\n");
	
	if (biggestAreaId > -1) {
		printf("Biggest area: %d with number %d.\n", biggestAreaId, result);
		printf("%d is number of line (counted from 1) in input file where coordinates with biggest area is placed.\n", biggestAreaId);
	}
	else {
		printf("No area left for counting\n");
	}
	

	// clean
	printf("clean memory... \n");
	freeMemory(coordinatesArray, sizeCA);
	freeMemory(matrix, maxX);
	printf("clean memory done.\n\n");

	system("pause");
	return 0;
}